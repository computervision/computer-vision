# Computer Vision
## Homework

This is a project of students of *Electrical Engineering and Information Technology* at [Technical University of Munich](http://www.tum.de) (TUM).
The Project is part of [LDV's](https://www.ldv.ei.tum.de/) course [Computer Vision](https://www.ldv.ei.tum.de/lehre/computer-vision/).

### Links:
Zusammenfassung der Videovorlesungen: [repository](https://bitbucket.org/computervision/computer-vision-summary)

### Written by:

-   Dominik Füß
-   Markus Hofbauer
-   Kevin Meyer
-   Florian Wirnshofer