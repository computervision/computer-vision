% Gruppenmitglieder: (D01) 
% Füß, Dominik
% Hofbauer, Markus
% Meyer, Kevin
% Wirnshofer, Florian

%% Merkmalsextraktion
Image1 = imread('szeneL.jpg');
IGray1 = rgb_to_gray(Image1);
Merkmale1 = harris_detektor(IGray1,'do_plot',false);
 
Image2 = imread('szeneR.jpg');
IGray2 = rgb_to_gray(Image2);
Merkmale2 = harris_detektor(IGray2,'do_plot',false);
 
%% Korrespondenzschätzung
Korrespondenzen = punkt_korrespondenzen(IGray1,IGray2,Merkmale1,Merkmale2,'do_plot',true);
