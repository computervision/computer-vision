function [Ix,Iy]=sobel_xy(I)
% In dieser Funktion soll das Sobel-Filter implementiert werden, welches
% ein Graustufenbild einliest und den Bildgradienten in x- sowie in
% y-Richtung zurückgibt.

% Interpolationsfilter
di = [1  2 1];

% Ableitungsfilter
dd = [1 0 -1];

% Ausnutzen der Separabilität des Sobel-Filters. 
Ix=conv2(di,dd,I,'same');
Iy=conv2(dd,di,I,'same');

end

