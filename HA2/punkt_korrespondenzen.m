function [Korrespondenzen]=punkt_korrespondenzen(I1,I2,Mpt1,Mpt2,varargin)

%% Input Parser
p=inputParser;

addRequired(p, 'I1', @(x) size(x,3) == 1 && isnumeric(x));
addRequired(p, 'I2', @(x) size(x,3) == 1 && isnumeric(x));
addRequired(p, 'Mpt1', @(x) size(x,1) == 2 && isnumeric(x));
addRequired(p, 'Mpt2', @(x) size(x,1) == 2 && isnumeric(x));
addOptional(p, 'do_plot', false, @(x) islogical(x));
addOptional(p, 'window_length', 30, @(x) x>1 && isscalar(x));
addOptional(p, 'min_corr', 0.875, @(x) x<1 && x>0 && isscalar(x));

p.parse(I1,I2,Mpt1,Mpt2,varargin{:});

do_plot         = p.Results.do_plot;
window_length   = p.Results.window_length;
min_corr        = p.Results.min_corr;

% Start Time Measurement
tic

% Convert image to double
I1_d=im2double(I1);
I2_d=im2double(I2);


%% Create patches with normalized gradient orientation and compensated gain and bias
[patches1,~] = rotationAssignment(I1_d, Mpt1, window_length);
[patches2,~] = rotationAssignment(I2_d, Mpt2, window_length);


%% Calculate NCC
N = size(patches1,1)*size(patches1,2);
Korrespondenzen = [];
for i = 1:size(Mpt1,2)
    for j = 1:size(Mpt2,2)
        A=patches1(:,:,i);
        B=patches2(:,:,j);
        A = A(:);
        B = B(:);
        NCC = A'*B/(N-1);
        % Neglect correspondances less than min_corr
        if NCC > min_corr
            Korrespondenzen(:,end+1) = [Mpt1(:,i);Mpt2(:,j);NCC];
            %             figure
            %             subplot(1,2,1)
            %             imshow(patches1(:,:,i))
            %             hold on
            %             subplot(1,2,2)
            %             imshow(patches2(:,:,j))
        end
    end
end

%% Remove double entries with lower NCC
Korrespondenzen = removeEntries(Korrespondenzen);

%% Display time and number of found pairs
time = toc;
display(sprintf('Anzahl gefundener Korrespondenzen: %d', size(Korrespondenzen, 2)))
display(sprintf('Zeit zur Berechnung: %f Sekunden', time))

%% Illustrate Results
if do_plot
    [rs1,cols] = size(I1_d);
    r2 = size(I2_d,1);
    if (rs1 < r2)
        I1_d(r2,1) = 0;
    else
        I2_d(rs1,1) = 0;
    end
    
    figure()
    % Display images side by side
    im = [I1_d I2_d];
    imshow(im)
    hold on
    
    for i = 1 : size(Korrespondenzen,2)
        line([Korrespondenzen(1,i), Korrespondenzen(3,i)+cols],[Korrespondenzen(2,i), Korrespondenzen(4,i)], 'Color', 'red')
        plot(Korrespondenzen(1,i),Korrespondenzen(2,i),'g.', 'MarkerSize', 20)
        plot(Korrespondenzen(3,i)+cols,Korrespondenzen(4,i),'g.', 'MarkerSize', 20)
    end
end
end

function [rotInvariantPatches,rotations] = rotationAssignment(image, featureLocations, windowLength)
[imageHeight,imageWidth] = size(image);
numberOfFeaturePoints = size(featureLocations,2);

% Preallocate storage for faster indexing
rotInvariantPatches = zeros(floor(windowLength/2)*2+1,floor(windowLength/2)*2+1,numberOfFeaturePoints);
rotations = zeros(numberOfFeaturePoints,1);

% Image Patch required for rotation needs to be bigger than windowLength by
% factor of sqrt(2) (square window)
halfWindow = ceil(windowLength*sqrt(2)/2);

%% Orientation Assignment
% Create histogram bins for gradient orientation in neighbourhood of feature
histogrambins =0:5:360;

featureNeighberhood = 3; % Important Tuning Parameter (odd number)

% Filter Image to retrieve L
[ X, ~ ] = meshgrid(-3:3,-3:3);
dGdx = -X .* gaussianWindow(3,1);
Ix = imfilter(image, dGdx,  'same');
Iy = imfilter(image, dGdx', 'same');

% Calculate Orientation and Magnitude
gradientMagnitude = sqrt(Ix.^2 + Iy.^2);
gradienOrientation = atan2d( Iy, Ix );
gradienOrientation(gradienOrientation<0)=gradienOrientation(gradienOrientation<0)+360;

% Create Gaussian window for smoothening magnidudes
window = gaussianWindow(featureNeighberhood,1.5);

for i=1:numberOfFeaturePoints
    % Feature Location
    x = featureLocations(1,i);
    y = featureLocations(2,i);
    
    % Neglect Patches that exeed the Image borders
    if ( isWithinPictureRange(x,y,halfWindow,imageHeight,imageWidth))
        rotations(i) = NaN;
        continue
    end
    
    % Create Pixle Neighbourhood around feature point (sqrt(2) times bigger
    % than windowLength
    rotationPatch = image(y-halfWindow:y+halfWindow,x-halfWindow:x+halfWindow);
    
    % Preallocate orientation histogram for orientations
    values=zeros(length(histogrambins)-1,1);
    regionOfInterestOrientation = gradienOrientation(y-featureNeighberhood:y+featureNeighberhood,x-featureNeighberhood:x+featureNeighberhood);
    % Smoothen magnitude response towards edges of neighbourhood
    regionOfInterestMagnitude =  gradientMagnitude(y-featureNeighberhood:y+featureNeighberhood,x-featureNeighberhood:x+featureNeighberhood).*window;
    regionOfInterestMagnitude = regionOfInterestMagnitude(:);
    % Fill histogram with corresponding magnitudes
    [~,ind] = histc(regionOfInterestOrientation,histogrambins);
    ind = ind(:);
    for j = 1:length(ind)
        values(ind(j))=values(ind(j))+regionOfInterestMagnitude(j);
    end
    % Extract maximum gradient in orientation histogram
    [~,index] = max(values);
    % Map 0-360 deg rotation to -180-0-180 deg
    rotation = -atan2d(sind(index*5-2.5),cosd(index*5-2.5));
    rotations(i) = -rotation;
    % Rotate the image about macimum gradient orientation
    rotationPatch = imrotate(rotationPatch, rotation, 'bilinear', 'crop');
    % Determine center of "oversiced" Patch
    cXY = halfWindow + 1;
    % Extract center of patch that has dimension 
    % windowLength x windowLength
    rotationPatch = rotationPatch(cXY-floor(windowLength/2):cXY+floor(windowLength/2),cXY-floor(windowLength/2):cXY+floor(windowLength/2));
    
    %% Compensate Bias and Gain
    m = mean(rotationPatch(:));
    sd = std(rotationPatch(:));
    rotInvariantPatches(:,:,i) = (rotationPatch-m)./sd;
end
end

function window = gaussianWindow(windowLength,sigma)
% Creates 2D Gaussian Window with stdev = sigma
x  = -windowLength:windowLength;
[ X, Y ] = meshgrid(x,x);
window = exp(-( X.*X + Y.*Y )/(2*sigma^2)) ./ (2*pi*sigma^2);
end

function boolInPicture = isWithinPictureRange(x,y,halfTileSize,imageHeight,imageWidth)
% Determine if Image Patch tries to acces values out of image bounds
if ((y-halfTileSize<1) || ((y+halfTileSize)>imageHeight) ...
        || ((x-halfTileSize)<1) || ((x+halfTileSize)>imageWidth))
    boolInPicture = true;
else
    boolInPicture = false;
end
end

function Korrespondenzen = removeEntries(KorrespondencenNcc)
% Removes double matches ordered by priority
Korrespondenzen = [];
if ~isempty(KorrespondencenNcc)
    ncc_list = KorrespondencenNcc(end,:);
    corPoints = KorrespondencenNcc(1:4,:);
    for a=1:size(corPoints,2)-1
        for b=a+1:size(corPoints,2)
            if ((corPoints(1, a)==corPoints(1, b)&&corPoints(2, a)==corPoints(2, b))||...
                    (corPoints(3, a)==corPoints(3, b)&&corPoints(4, a)==corPoints(4, b)))
                if ncc_list(a)>ncc_list(b)
                    corPoints(:, b)=[0; 0; 0; 0];
                else
                    corPoints(:, a)=[0; 0; 0; 0];
                end
            end
        end
    end
    Korrespondenzen = corPoints;
    Korrespondenzen( :, all(~Korrespondenzen,1) ) = [];
end
end
