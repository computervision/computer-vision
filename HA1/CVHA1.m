% Gruppenmitglieder: (D01) 
% Fuchs, Julian
% Füß, Dominik
% Hofbauer, Markus
% Meyer, Kevin
% Wirnshofer, Florian

% Für die letztendliche Abgabe bitte die Kommentare in den folgenden Zeilen
% enfernen und sicherstellen, dass alle optionalen Parameter über den
% entsprechenden Funktionsaufruf fun('var',value) modifiziert werden können.

close all
clear all 
clc

Image = imread('szene.jpg');
IGray = rgb_to_gray(Image);
tic
Merkmale = harris_detektor(IGray,'do_plot',true);
toc

display(['Anzahl der gefunden Merkmale: ',num2str(length(Merkmale))])
