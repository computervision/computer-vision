% Gruppenmitglieder: (D01) 
% Füß, Dominik
% Hofbauer, Markus
% Meyer, Kevin
% Wirnshofer, Florian

%% Merkmalsextraktion
Image1 = imread('szeneL.jpg');
IGray1 = rgb_to_gray(Image1);
Merkmale1 = harris_detektor(IGray1,'do_plot',false,'N',100,'min_dist',5,'window_length',9);

Image2 = imread('szeneR.jpg');
IGray2 = rgb_to_gray(Image2);
Merkmale2 = harris_detektor(IGray2,'do_plot',false,'N',100,'min_dist',5,'window_length',9);

%% Korrespondenzschätzung

Korrespondenzen = punkt_korrespondenzen(IGray1,IGray2,Merkmale1,Merkmale2,'do_plot',false);

%% Extraktion der Essentiellen Matrix aus robusten Korrespondenzen
tic
Korrespondenzen_robust = F_ransac(Korrespondenzen);
toc

load K;
E = achtpunktalgorithmus(Korrespondenzen_robust,K);
display(E)

%% Visualisierung
disp(['Anzahl der robusten Korrespondenzen: ',num2str( size(Korrespondenzen_robust,2))])

figure
% Display images side by side
im = [IGray1 IGray2];
[rs1,cols] = size(IGray1);
imshow(im)
hold on

%%%%%% Optional: Anzeigen aller gefundenen Korrespondenzen
% for i = 1 : size(Korrespondenzen,2)
%     plot(Korrespondenzen(1,i),Korrespondenzen(2,i),'r*')
%     plot(Korrespondenzen(3,i)+cols,Korrespondenzen(4,i),'r*')
% end
    
for i = 1 : size(Korrespondenzen_robust,2)
    line([Korrespondenzen_robust(1,i), Korrespondenzen_robust(3,i)+cols],[Korrespondenzen_robust(2,i), Korrespondenzen_robust(4,i)], 'Color', 'red')
    plot(Korrespondenzen_robust(1,i),Korrespondenzen_robust(2,i),'g.', 'MarkerSize', 20)
    plot(Korrespondenzen_robust(3,i)+cols,Korrespondenzen_robust(4,i),'g.', 'MarkerSize', 20)
end
