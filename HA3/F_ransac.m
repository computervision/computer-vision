function [Korrespondenzen_robust]=F_ransac(Korrespondenzen,varargin)
%% Input Parser
parser=inputParser;

addRequired(parser, 'Korrespondenzen', @(x) size(x,1) == 4 && isnumeric(x));
addOptional(parser, 'epsilon', 0.3, @(x) isscalar(x) && x>=0 && x<=1);
addOptional(parser, 'p', 0.95, @(x) isscalar(x) && x>=0 && x<=1);
addOptional(parser, 'tolerance', 0.1, @(x) isscalar(x));

parser.parse(Korrespondenzen,varargin{:});

epsilon         = parser.Results.epsilon;
p               = parser.Results.p;
tolerance       = parser.Results.tolerance;

%% Calculation
% Number of required runs to ensure given P
numberOfKorr = size(Korrespondenzen,2);
s = ceil(log(1-p) / log(1-(1-epsilon)^8));

% Init Vars
Korrespondenzen_robust = [];
sumError = 0.0;

for x = 1:s
    randomKorrInds = randperm(numberOfKorr,8);
    randomKorrs = Korrespondenzen(:,randomKorrInds);
    curF = achtpunktalgorithmus(randomKorrs);
    curA = getSetOfFittingKorrs(curF, Korrespondenzen, tolerance);
    if size(Korrespondenzen_robust,2) < size(curA,2)
        Korrespondenzen_robust = curA;
        sumError = getSumOfSampsonErrorOfInliers(curA, curF);
    elseif size(Korrespondenzen_robust,2) == size(curA,2) && size(curA,2) > 0
        curErrorSum = getSumOfSampsonErrorOfInliers(curA, curF);
        if curErrorSum < sumError
            Korrespondenzen_robust = curA;
            sumError = curErrorSum;
        end
    end
end
end

function a = getSetOfFittingKorrs(F, Korrespondenzen, tolerance)
%%
a = zeros(size(Korrespondenzen));
x1 = [Korrespondenzen(1:2,:);ones(1,size(Korrespondenzen,2))];
x2 = [Korrespondenzen(3:4,:);ones(1,size(Korrespondenzen,2))];
a_pos=1;
for i = 1:size(Korrespondenzen,2)
    if getSampsonError(x1(:,i), x2(:,i), F) < tolerance
        a(:, a_pos)=[x1(1:2,i);x2(1:2,i)];
        a_pos=a_pos+1;
    end
end
a=a(:,any(a));
end

function se = getSumOfSampsonErrorOfInliers(A, F)
%% Sum of Costfunction
se = 0.0;
x1 = [A(1:2,:);ones(1,size(A,2))];
x2 = [A(3:4,:);ones(1,size(A,2))];
for j = 1:size(A,2)
    se = se + getSampsonError(x1(:,j), x2(:,j), F);
end
end

function e = getSampsonError(x1, x2, F)
%% Sampson Error with Fundamental Matrix
e3hat = [0,-1,0;1,0,0;0,0,0];
e = ((x2' * F * x1)^2) / ((norm(e3hat * F * x1))^2 + (norm(x2' * F * e3hat))^2);
% Check Formula!
end