function [EF]=achtpunktalgorithmus(Korrespondenzen,K)

assert(size(Korrespondenzen,2)>=8);
assert(size(Korrespondenzen,1)==4);

%% Solve minimization problem
if ~exist('K','var') || isempty(K)
    A = createA(Korrespondenzen);
    EF = getF(A);
else
    % calculate calibrated correspondence points
    Korrespondenzen_kalibriert = calibrate(Korrespondenzen,K);
    A = createA(Korrespondenzen_kalibriert);
    EF = getE(A);
end

end

function A = createA(Korrespondenzen)
%% Vectorization
A = zeros(size(Korrespondenzen,2),9);
x_1 = [Korrespondenzen(1:2,:);ones(1,size(Korrespondenzen,2))];
x_2 = [Korrespondenzen(3:4,:);ones(1,size(Korrespondenzen,2))];
for i=1:size(Korrespondenzen,2)
    a = kron(x_1(:,i),x_2(:,i));
    A(i,:) = a';
end
end

function Korrespondenzen_kalibriert = calibrate(Korrespondenzen,K)
x_1 = [Korrespondenzen(1:2,:);ones(1,size(Korrespondenzen,2))];
x_2 = [Korrespondenzen(3:4,:);ones(1,size(Korrespondenzen,2))];
x_1 = K\x_1;
x_2 = K\x_2;
Korrespondenzen_kalibriert = [x_1(1:2,:);x_2(1:2,:)];
end

function G = createG(A)
    [~,~,V] = svd(A); 
    Gs = V(:,9);
    G = [Gs(1:3),Gs(4:6),Gs(7:9)];
end

function E = getE(A)
    G = createG(A); 
    [U,Sigma,V] = svd(G);
    sigma = diag(Sigma);
    sigma = mean(sigma(1:2));
    Sigma = diag([sigma, sigma, 0]);
    E = U*Sigma*V';
end

function F = getF(A)
    G = createG(A); 
    [U,Sigma,V] = svd(G);
    Sigma(end) = 0;
    F = U*Sigma*V';
end