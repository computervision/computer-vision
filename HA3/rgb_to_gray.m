function [Image]=rgb_to_gray(Input)
% Diese Funktion soll ein RGB-Bild in ein Graustufenbild umwandeln. Falls
% das Bild bereits in Graustufen vorliegt, soll es zurückgegeben werden.

% Überprüfe, ob das Bild tatsächlich drei Kanäle hat
if(size(Input,3)~=3)
    Image=Input;
    return;
end
% Umwandlung nach ITU-Formel
Image = 0.299*Input(:,:,1)+0.587*Input(:,:,2)+0.114*Input(:,:,3);

end
