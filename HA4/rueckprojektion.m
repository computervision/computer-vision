function repro_error=rueckprojektion(Korrespondenzen, P1, Image2, T, R, K)

x2_strich = [Korrespondenzen(3:4,:);ones(1,size(Korrespondenzen,2))];
pi0 = eye(3,4);
 
x2_repro = zeros(size(x2_strich));
RT = [R T; zeros(1, 3) 1];

for a=1:size(x2_strich, 2)
    x2_repro(:, a)=K*pi0*RT*[P1(:, a); 1];
end

x2_repro(1,:) = x2_repro(1,:)./x2_repro(3,:);
x2_repro(2,:) = x2_repro(2,:)./x2_repro(3,:);
x2_repro(3,:) = x2_repro(3,:)./x2_repro(3,:);

% Visualisierung
figure('name', 'Reprojection Error');
imshow(Image2)
hold on;
for i = 1 : size(Korrespondenzen,2)
    plot(Korrespondenzen(3,i),Korrespondenzen(4,i),'gO', 'MarkerSize', 7)
    plot(x2_repro(1,i),x2_repro(2,i),'rO', 'MarkerSize', 7)
    M=0.5*([Korrespondenzen(3,i),Korrespondenzen(4,i)]+[x2_repro(1,i),x2_repro(2,i)]);
    line([Korrespondenzen(3,i) M(1)], [Korrespondenzen(4,i) M(2)], 'Color', 'g')
    line([M(1) x2_repro(1,i)], [M(2) x2_repro(2,i)], 'Color', 'r')
end
l=legend('Correct Point', 'Reconstructed Point', 'Location', 'se');
set(l, 'Box', 'off')
hold off;

% Berechnung der Rueckprojektionsfehlers
fehler = x2_strich-x2_repro;
repro_error = mean(diag(sqrt(fehler'*fehler)));

end