function [T1,R1,T2,R2]=TR_aus_E(E)

[U,S,V] = svd(E);

U=U*[1 0 0; 0 1 0; 0 0 det(U)]; 
V=V*[1 0 0; 0 1 0; 0 0 det(V)];

R = [0 -1 0; 1 0 0; 0 0 1];

That = U*R*S*U';
T1 = getTFromTHat(That);
R1 = U*R'*V'; 
That = U*R'*S*U';
T2 = getTFromTHat(That);
R2 = U*R*V'; 

end

function T = getTFromTHat(THat)

T = [THat(3,2);THat(1,3);THat(2,1)];

end