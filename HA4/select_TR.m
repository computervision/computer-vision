function [T,R, lambda] = select_TR(T1,T2,R1,R2, Korrespondenzen, K)

% Speichere die Ts und Rs in einem Array ab
T_all = cat(2, T1, T2, T1, T2);
R_all = cat(3, R1, R1, R2, R2);

N = length(Korrespondenzen);
x1 = [Korrespondenzen(1:2,:); ones(1, N)];
x2 = [Korrespondenzen(3:4,:); ones(1, N)];

x1 = K \ x1;
x2 = K \ x2;

% 4 mögliche Kombinationen aus T_1,2 und R_1,2
lambdas = zeros(N, 2, 4);
posdepth = zeros(1,4);
for i=1:4
    M1 = zeros(3*N,N+1);
    M2 = zeros(3*N,N+1);
    
    for j=1:N        
        M1((j-1)*3+1:(j-1)*3+3,j)    = skew(x2(:,j)) * R_all(:,:,i) * x1(:,j);
        M1((j-1)*3+1:(j-1)*3+3,N+1)  = skew(x2(:,j)) * T_all(:,i);
        
        M2((j-1)*3+1:(j-1)*3+3,j)    = skew( R_all(:,:,i) * x1(:,j) ) * x2(:,j);
        M2((j-1)*3+1:(j-1)*3+3,N+1)  = -skew( R_all(:,:,i) * x1(:,j) ) * T_all(:,i);
    end
    
    % Least-Squares-Lösung via Singulärwertzerlegung
    [~,~,V1] = svd(M1,0);
    lambdas1 = V1(:,N+1);
    lambdas1 = lambdas1 ./ lambdas1(end);
    [~,~,V2] = svd(M2,0);
    lambdas2 = V2(:,N+1);
    lambdas2 = lambdas2 ./ lambdas2(end);    
    
    % Summiere über die Vorzeichen der Tiefen
    posdepth(i) = sum(sign(lambdas1(1:end-1))) + sum(sign(lambdas2(1:end-1)));
    lambdas(:, 1, i)= lambdas1(1:end-1);
    lambdas(:, 2, i)= lambdas2(1:end-1);
end

% Finde die Konfiguration, bei der die meisten Tiefen positiv sind
[~, index] = max(posdepth);

% Wähle das richtige Paar (R,T) aus
T       = T_all(:,index);
R       = R_all(:,:,index);
lambda  = lambdas(:, :, index);

end

function [Vhat]=skew(V)
%Umwandlung von V in eine schiefsymmetrische Matrix
Vhat = [0 -V(3) V(2); V(3) 0 -V(1); -V(2) V(1) 0];
end