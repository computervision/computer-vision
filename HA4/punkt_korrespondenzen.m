function [Korrespondenzen]=punkt_korrespondenzen(I1,I2,Mpt1,Mpt2,varargin)

%% Input parser
P = inputParser;

P.addRequired('I1', @isnumeric);
P.addRequired('I2', @isnumeric);
P.addRequired('Mpt1', @isnumeric);
P.addRequired('Mpt2', @isnumeric);

% Liste der optionalen Parameter
% Fensterlänge
P.addOptional('window_length', 25, @isnumeric)
% Minimal geforderte Korrelation
P.addOptional('min_corr', 0.9, @(x)isnumeric(x) && x > 0 && x < 1);
% Plot ein/aus
P.addOptional('do_plot', false, @islogical);

% Lese den Input
P.parse(I1,I2,Mpt1,Mpt2,varargin{:});

% Extrahiere die Variablen aus dem Input-Parser
window_length = P.Results.window_length;
min_corr = P.Results.min_corr;
do_plot = P.Results.do_plot;

% Anzahl der Merkmale in den Bildern
no_pts1 = size(Mpt1,2);
no_pts2 = size(Mpt2,2);

Offset = floor(window_length/2);

% Erweitere das Bild um einen Nullrand der Breite W/2, damit auch Punkte am
% Rand verglichen werden könnten
Im1 = zeros(size(I1,1)+window_length-1,size(I1,2)+window_length-1);
Im2 = zeros(size(I2,1)+window_length-1,size(I2,2)+window_length-1);
Im1(Offset+1:end-Offset,Offset+1:end-Offset) = I1;
Im2(Offset+1:end-Offset,Offset+1:end-Offset) = I2;

% Zunächst müssen wir die Indizes aller Punkte im Suchfenster bestimmen.
% Nachdem alle Bildkoordinaten durch den Nullrand um W/2 verschoben wurden,
% hat die äußerste Ecke des Fensters genau die Koordinaten des
% Merkmalspunktes. Die anderen Einträge haben einen entsprechenden Offset
Win_Ix = 0:window_length-1;

% Jetzt extrahieren wir alle Merkmalsregionen. Es gilt tr(B'A) = vec(B)'*vec(A),
% wobei vec(.) ein Operator ist, der eine Matrix vektorisiert. Da das
% Produkt von zwei Vektoren schneller zu berechnen ist als die Spur eines
% Produkts zweier Matrizen, speichern wir die Regionen um die Merkmalspunkte
% als Vektor ab

%Matrix mit allen vektorisierten Merkmalsregionen in Bild 1
Mat_feat_1 = zeros(window_length*window_length,no_pts1);
%Matrix mit allen vektorisierten Merkmalsregionen in Bild 2
Mat_feat_2 = zeros(window_length*window_length,no_pts2);

for index = 1:max(no_pts1,no_pts2)
    if index <= no_pts1
        % Verschiebung der Indizes an korrekte Position
        W_x_coord = Win_Ix + Mpt1(1,index);
        W_y_coord = Win_Ix + Mpt1(2,index);
        % Fenster extrahiert;
        W_extracted = Im1(W_y_coord,W_x_coord);
        % Normierung und Abspeichern als Vektor
        Mat_feat_1(:,index) = (W_extracted(:)-mean(W_extracted(:))) / std(W_extracted(:));
    end
    
    if index <= no_pts2
        % Verschiebung der Indizes an korrekte Position
        W_x_coord = Win_Ix + Mpt2(1,index);
        W_y_coord = Win_Ix + Mpt2(2,index);
        % Fenster extrahiert;
        W_extracted = Im2(W_y_coord,W_x_coord);
        % Normierung und Abspeichern als Vektor
        Mat_feat_2(:,index) = (W_extracted(:)-mean(W_extracted(:))) / std(W_extracted(:));
    end
end

% Hier wird die letztendliche NCC berechnet durch ein einfaches
% Matrix-Matrix-Produkt. Im Eintrag x,y der NCC_matrix steht die Korrelation des Punktes X
% im zweiten Bild mit dem punkt y im ersten Bild
NCC_matrix = 1/(window_length*window_length-1)*Mat_feat_2'*Mat_feat_1;

% Setze alle Korrelationswerte auf 0, die kleiner als der Schwellwert sind
NCC_matrix(NCC_matrix<min_corr)=0;

% Sortiere die Einträge der Matrix
[sorted_list,sorted_index] = sort(NCC_matrix(:),'descend');

% Eliminiere Elemente aus der Liste, deren Korrelationswert auf null
% gesetzt wurde
sorted_index(sorted_list==0)=[];

% Initialisere die Korrespondenzenmatrix
Korrespondenzen=zeros(4,min(no_pts1,no_pts2));
Korr_count=1;

%Maximale Anzahl an iterationen entspricht
max_it = numel(sorted_index);
size_ncc = size(NCC_matrix);
% Falls eine grafische Darstellung gewünscht, stelle zuerst I1 und I2
% nebeneinander dar
if do_plot
    figure(1)
    imshow(uint8([I1,I2]));
    % Offset für die Punkte im zweiten Bild
    xoffset = size(I1,2);
    hold on
end
for it = 1:max_it
    
    % Nehme nächstes Element aus der absteigend nach Größe sortierten Liste
    pt_index = sorted_index(it);
    % Kontrolle ob dieser Wert noch existiert oder bereits auf 0 gesetzt
    % wurde
    if(NCC_matrix(pt_index)==0)
        continue;
    else
        %Extrahiere Reihen- und Spaltenindex
        [Idx_fpt2,Idx_fpt1]=ind2sub(size_ncc,pt_index);
    end
    
    %Setze entsprechende Spalte gleich null, damit ein
    %Merkmalspunkt in Bild 1 nicht mehr als einem anderen Merkmalspunkt in
    % Bild 2 zugewiesen werden kann.
    NCC_matrix(:,Idx_fpt1) = 0;
    % Speichere das Korrespondenzpunktpaar ab
    Korrespondenzen(:,Korr_count) = [Mpt1(:,Idx_fpt1);Mpt2(:,Idx_fpt2)];
    Korr_count = Korr_count+1;
    if do_plot
        %Zeichne KP in Bild 1 ein
        plot(Mpt1(1,Idx_fpt1),Mpt1(2,Idx_fpt1),'o');
        %Zeichne KP in Bild 2 ein
        plot(Mpt2(1,Idx_fpt2)+xoffset,Mpt2(2,Idx_fpt2),'o');
        %Verbinde beide Punkte mit einer Linie
        line([Mpt1(1,Idx_fpt1),Mpt2(1,Idx_fpt2)+xoffset],[Mpt1(2,Idx_fpt1),Mpt2(2,Idx_fpt2)],'Color',[0,0,0]);
    end
end
% Lösche überflüssige Elemente der Korrespondenzenmatrix
Korrespondenzen = Korrespondenzen(:,1:Korr_count-1);




