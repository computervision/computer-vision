function [T,R, lambdas, P1] = rekonstruktion(T1,T2,R1,R2, Korrespondenzen, K)

% Berechnung der richtigen Euklidischen Bewegung
[T,R,lambdas] = select_TR(T1,T2,R1,R2,Korrespondenzen,K);

% Berechnung der 3D-Punkte in Bild 1
x1 = [Korrespondenzen(1:2,:);ones(1,size(Korrespondenzen,2))];
lambda1 = lambdas(:,1)';
x1 = K\x1;
P1 = [lambda1.*x1(1,:); lambda1.*x1(2,:); lambda1.*x1(3,:)];

Camera1 = [0;0;0];
Camera2 = -R\T;

% Visualisierung
figure('name', 'Reconstructed 3D_Points');
p1 = plot3(P1(1,:),P1(3,:),P1(2,:),'bo');
hold on;

normal1 = [0;0;1];
normal2 = R\normal1;
p2 = plotCamera(Camera1,normal1,'r');
p3 = plotCamera(Camera2,normal2,'g');

xlabel('x')
ylabel('z')
zlabel('y')

axis equal
ylim([-0.1 max(P1(3,:))])
grid on

l = legend([p1,p2,p3],'3D-Points', 'Camera 1', 'Camera 2', 'Location', 'sw');
set(l, 'Box', 'off')
hold off;

end

function lengendentry = plotCamera(center,normal,color)

offsetX = 0.15;
offsetY = 0.1;
offsetZ = -(normal(1)*offsetX + normal(2)*offsetY)/normal(3);
lengendentry = plot3(center(1),center(3),center(2),color,'Marker','x','LineStyle','none','LineWidth',2.0,'MarkerSize',10.0);
plot3([center(1)-offsetX,center(1)+offsetX,center(1)+offsetX,center(1)-offsetX,center(1)-offsetX], ...
    [center(3)-offsetZ,center(3)+offsetZ,center(3)+offsetZ,center(3)-offsetZ,center(3)-offsetZ], ...
    [center(2)-offsetY,center(2)-offsetY,center(2)+offsetY,center(2)+offsetY,center(2)-offsetY],color,'LineWidth',2.5);
plot3([center(1),center(1)+normal(1)],[center(3),center(3)+normal(3)],[center(2),center(2)+normal(2)],color,'LineWidth',2.5);

end