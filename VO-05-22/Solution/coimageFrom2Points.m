function l=coimageFrom2Points(P1,P2)

P1_hom=[P1(1);P1(2);1];
P2_hom=[P2(1);P2(2);1];

l=cross(P1_hom,P2_hom);

end