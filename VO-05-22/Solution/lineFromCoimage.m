function lineFromCoimage(l,I)

X1=0;
X2=size(I,2);

Y1 = -l(3)/l(2);
Y2 = -(X2*l(1)+l(3))/l(2);

figure
imagesc(I,[0 255])
colormap gray
line([X1;X2],[Y1;Y2])

end