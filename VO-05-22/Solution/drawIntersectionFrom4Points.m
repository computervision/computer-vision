function drawIntersectionFrom4Points(P1,P2,P3,P4,I)

l12=coimageFrom2Points(P1,P2);
l34=coimageFrom2Points(P3,P4);

l = cross(l12,l34);
l=l./l(3);

drawLineFromCoimage(l12,I);
drawLineFromCoimage(l34,I);

scatter(l(1),l(2),'r')

end