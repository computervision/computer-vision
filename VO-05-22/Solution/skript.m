%% Initialisierung

I=imread('I1.png');

figure
imshow(uint8(I))
hold on

%% Aufgabe 1
l1=[1;-10;740];
l2=[2;5;-1470];

drawLineFromCoimage(l1,I);
drawLineFromCoimage(l2,I);

%% Aufgabe 2
p1=[125;214];
p2=[200;354];

l12=coimageFrom2Points(p1,p2);

drawLineFromCoimage(l12,I);

%% Aufgabe 3
p3=[124;150];
p4=[292;118];

drawIntersectionFrom4Points(p1,p2,p3,p4,I)

%% Aufgabe 4
p5=[244;191];
drawRandomCrossingLines(p5,I)

%% Aufgabe 5 (Visualisierung)
p6=[68;255];
p7=[131;399];
p8=[35;109];
p9=[113;358];
p10=[54;223];

scatter(p6(1),p6(2))
scatter(p7(1),p7(2))
scatter(p8(1),p8(2))
scatter(p9(1),p9(2))
scatter(p10(1),p10(2))

%% Aufgabe 5 (analytisch)
A1=[p6 p7 p8 p9;ones(1,4)];
[~,Sigma1,~]=svd(A1);
sigma1=Sigma1(3,3)

A2=[p7 p8 p9 p10;ones(1,4)];
[~,Sigma2,~]=svd(A2);
sigma2=Sigma2(3,3)

A3=[p6 p7 p9 p10;ones(1,4)];
[~,Sigma3,~]=svd(A3);
sigma3=Sigma3(3,3)
















