function aufgaben()
%% Voll die Beschreibung, so
%% aufgabe1
Image = imread('I1.png');

l1=[1;-10;740];
l2 =[2;5;-1470];

[X1,m1] = aufgabe1(l1);
[X2,m2] = aufgabe1(l2);

breite = size(Image,2);

figure
imshow(Image);
hold on
line([X1(1); breite],[X1(2); m1*breite + X1(2)]);
line([X2(1); breite],[X2(2); m2*breite + X2(2)]);


%% aufgabe2

p1= [125;214];
p2= [200;354];

l12 = cross([p1;1],[p2;1]);
[X12,m12] = aufgabe1(l12);

line([X12(1); breite],[X12(2); m12*breite + X12(2)]);
plot([p1(1),p2(1)], [p1(2),p2(2)], 'g.', 'MarkerSize', 20);

%% aufgabe3

p3= [124;150];
p4= [292;118];

aufgabe3(p1, p2, p3, p4, breite);

end

function [x, m] = aufgabe1(l)

x1 = -l(3)/l(1);
x2 = 0;
x3 = 0;
x4 = -l(3)/l(2);

m=(x2-x4)/(x1-x3);

x = [x3; x4];

end

function aufgabe3(p1, p2, p3, p4, breite)
l12 = cross([p1;1],[p2;1]);
[X12,m12] = aufgabe1(l12);

l34 = cross([p3;1],[p4;1]);
[X34,m34] = aufgabe1(l34);

line([X12(1); breite],[X12(2); m12*breite + X12(2)]);
plot([p1(1),p2(1)], [p1(2),p2(2)], 'g.', 'MarkerSize', 20);

line([X34(1); breite],[X34(2); m34*breite + X34(2)]);
plot([p3(1),p4(1)], [p3(2),p4(2)], 'g.', 'MarkerSize', 20);

xSchnitt = cross(l12, l34);
xSchnitt = 1/xSchnitt(3) * xSchnitt;
plot(xSchnitt(1),xSchnitt(2), 'g.', 'MarkerSize', 20);


end